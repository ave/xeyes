from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import math


class XEyes(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._eye_size = 50
        self._eye_border_size = 5
        self._pupil_size = 9
        self._pupil_movement_multiplier = 4.5
        self._captouch_movement_multiplier_x = 11
        self._captouch_movement_multiplier_y = 15
        self._eye_safe_boundary = 85
        self._pupil_limit = (
            (((self._eye_size * 2) - (self._eye_border_size * 2)) - self._pupil_size)
            * (self._eye_safe_boundary / 100)
        ) / 2

        self._imu_y_left = 0
        self._imu_x_left = 0
        self._imu_y_right = 0
        self._imu_x_right = 0

    def calc_pupil_offset(self, x, y):
        tilt = x**2 + y**2
        pupil_offset_x = x * self._pupil_movement_multiplier
        pupil_offset_y = y * self._pupil_movement_multiplier

        # 6 = magic number
        if tilt >= (self._eye_safe_boundary - (self._pupil_size * 2) - 6):
            angle = math.atan2(pupil_offset_y, pupil_offset_x)
            pupil_offset_x = self._pupil_limit * math.cos(angle)
            pupil_offset_y = self._pupil_limit * math.sin(angle)

        return pupil_offset_x, pupil_offset_y

    def draw(self, ctx: Context) -> None:
        ctx.rgb(1, 1, 1).rectangle(-120, -120, 240, 240).fill()
        ctx.scale(1, 1 / 0.6)

        ctx.rgb(0, 0, 0)
        ctx.save()

        # eyes
        ctx.line_width = self._eye_border_size
        ctx.arc(59, 0, self._eye_size, 0, math.tau, 0).stroke()
        ctx.arc(-59, 0, self._eye_size, 0, math.tau, 0).stroke()

        # pupils
        pupil_offset_x_left, pupil_offset_y_left = self.calc_pupil_offset(
            self._imu_x_left, self._imu_y_left
        )
        pupil_offset_x_right, pupil_offset_y_right = self.calc_pupil_offset(
            self._imu_x_right, self._imu_y_right
        )

        ctx.arc(
            -(59 - pupil_offset_x_left),
            pupil_offset_y_left,
            self._pupil_size,
            0,
            math.tau,
            0,
        ).fill()
        ctx.arc(
            59 + pupil_offset_x_right,
            pupil_offset_y_right,
            self._pupil_size,
            0,
            math.tau,
            0,
        ).fill()

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._imu_y_left = self._imu_y_right = ins.imu.acc[0]
        self._imu_x_left = self._imu_x_right = ins.imu.acc[1]

        if ins.captouch.petals[6].pressed:
            petal_pos = ins.captouch.petals[6].position
            self._imu_y_left = (
                petal_pos[0] / 2**16
            ) * self._captouch_movement_multiplier_y
            self._imu_x_left = (
                petal_pos[1] / 2**16
            ) * -self._captouch_movement_multiplier_x

        if ins.captouch.petals[4].pressed:
            petal_pos = ins.captouch.petals[4].position
            self._imu_y_right = (
                petal_pos[0] / 2**16
            ) * self._captouch_movement_multiplier_y
            self._imu_x_right = (
                petal_pos[1] / 2**16
            ) * -self._captouch_movement_multiplier_x


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(XEyes(ApplicationContext()))
